/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wchar_type.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 17:29:36 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 19:23:01 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_wchar(va_list ap, t_flags flags)
{
	char	*tmp;
	char	*res;
	int		i;

	i = flags.type;
	tmp = (char *)va_arg(ap, wchar_t*);
	res = ft_strdup("\0");
	return (res);
}
