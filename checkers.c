/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkers.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 16:45:15 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 19:59:17 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	set_flags(t_flags *flags)
{
	flags->flag = ft_strdup("00000");
	flags->width = 0;
	flags->size = 0;
	flags->type = 0;
	flags->percent = 0;
	flags->nul = 0;
	flags->prec = -2;
}

int		get_lenght(char *format, int i, int percent)
{
	int res;

	res = 0;
	while (is_reading(format[++i]) == 1 && is_specifier(format[i]) != 1)
		res++;
	if (is_specifier(format[i]) == 1)
		res++;
	if (percent > 0)
		res++;
	return (res);
}

int		number_str_memory(char *format, int n)
{
	int	res;

	res = 0;
	while (ft_isdigit(format[n++]) == 1)
		res++;
	return (res);
}

char	*get_number(char *str, int start)
{
	char	*res;
	int		mem;
	int		n;

	n = -1;
	mem = number_str_memory(str, start);
	res = (char *)malloc(sizeof(char) * mem + 1);
	while (n < mem)
		res[++n] = str[start++];
	res[n] = '\0';
	return (res);
}

int		checker(char *format, int i, va_list ap, t_flags *flags)
{
	set_flags(flags);
//	check_width(format, i, flags);
//	check_precision(format, i, flags);
//	check_type(format, i, flags);
//	check_flag(format, i, flags);
//	check_size(format, i, flags);
	check_percent(format, i, flags);
	get_output_string(flags, ap);
	free(flags->flag);
	return (get_lenght(format, i, flags->percent));
}
