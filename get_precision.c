/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_precision.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 16:52:34 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 17:05:14 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	get_tmp(t_flags flags, char *tmp, int i)
{
	char	filler;

	if ((flags.type > 12 || flags.type < 3) && flags.flag[4] == '0')
		filler = ' ';
	else
		filler = '0';
	tmp[i] = '\0';
	while (--i >= 0)
		tmp[i] = filler;
}

char	*join(t_flags flags, char *tmp, char *str)
{
	char	*result;

	if (flags.type > 12 || flags.type < 3)
		result = ft_strjoin(str, tmp);
	else
		result = ft_strjoin(tmp, str);
	return (result);
}

char	*get_prec(char *str, t_flags flags, int p)
{
	char	*result;
	char	*tmp;
	int		i;
	int		n;

	if ((p == 0 && str[0] == '0') || str[0] == '\0')
		result = ft_strdup("\0");
	else
	{
		n = -1;
		if (str[0] == '-' && flags.type != 1)
			while (str[++n] != '\0')
				str[n] = str[n + 1];
		i = p - ft_strlen(str);
		i > 0 ? (tmp = (char *)malloc(sizeof(char) * i + 1)) :
			(tmp = ft_strdup("\0"));
		if ((int)ft_strlen(str) > p && flags.type == 2)
			str[p] = '\0';
		else if (i > 0)
			get_tmp(flags, tmp, i);
		(flags.type == 2 || flags.type == 1) ? (result = ft_strdup(str)) :
			(result = join(flags, tmp, str));
		free(tmp);
	}
	return (result);
}
