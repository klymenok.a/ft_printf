/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_output_string.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 17:11:37 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 19:23:03 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	free_strings(char **result)
{
	free(result[0]);
	free(result[1]);
	free(result[2]);
	free(result[3]);
	free(result[4]);
	if (result[5][0] != '\0')
		free(result[5]);
	free(result);
}

static char	*get_result(t_flags flags, char *res)
{
	char *result;

	if (((flags.type > 4 && flags.type < 9) || flags.type == 10) &&
			res[0] != '\0')
		result = convertor(ft_atoi(res), flags.type);
	else
		result = ft_strdup(res);
	return (result);
}

char		*get_value_to_string(t_flags flags, va_list ap)
{
	char	*res;
	char	*result;

	if (flags.type == 0)
		res = ft_strdup("\0");
	else if (flags.type == 1 || flags.type == 2)
		res = str_type(flags, ap);
	else if (flags.type == 13 || flags.type == 14)
		res = get_wchar(ap, flags);
	else if (flags.type == 3 || flags.type == 4 || flags.type == 9)
		res = signed_type_int(flags, ap);
	else if (flags.type == 8)
		res = pointer(ap);
	else if (flags.size == 0 && flags.type > 0)
		res = unsigned_type(flags, ap);
	else if (flags.size != 0)
		res = unsigned_type_with_size(flags, ap);
	else
		res = ft_strdup("\0");
	result = get_result(flags, res);
	free(res);
	return (result);
}

void		get_output_string(t_flags *flags, va_list ap)
{
	int		w;
	int		p;
	size_t	n;
	char	**res;

	res = (char **)malloc(sizeof(char *) * 5);
	w = 0;
	p = 0;
	flags->width == -1 ? (w = va_arg(ap, int)) :
		(w = flags->width);
	flags->prec == -1 ? (p = va_arg(ap, int)) :
		(p = flags->prec);
	res[0] = get_value_to_string(*flags, ap);
	if (ft_strcmp(res[0], "(null)") == 0 || ft_strcmp(res[0], "") == 0)
		flags->nul = 1;
	res[1] = get_sign(res[0], *flags);
	res[3] = get_last_symbol(flags->percent);
	res[4] = get_prec(res[0], *flags, p);
	res[5] = get_spec(*flags, res[4]);
	n = ft_strlen(res[4]) + ft_strlen(res[1]) + ft_strlen(res[3]) +
		ft_strlen(res[5]);
	res[2] = get_flags(*flags, n, res, w);
	put_strings(res, *flags);
	free_strings(res);
}
