/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_u.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 15:27:10 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 17:38:53 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	intlen(uintmax_t n)
{
	int		l;

	l = 0;
	if (n <= 0)
		l++;
	while (n != 0)
	{
		n = n / 10;
		l++;
	}
	return (l);
}

static char	*allocate_memory(uintmax_t i)
{
	char	*str;

	str = (char *)malloc(intlen(i) + 1);
	if (!str)
		return (NULL);
	return (str);
}

static char	*mini_itoa(char *str, int l, uintmax_t n)
{
	str[l] = '\0';
	while (l > 0)
	{
		str[--l] = n % 10 + '0';
		n = n / 10;
	}
	return (str);
}

char		*ft_itoa_u(uintmax_t n)
{
	char	*str;
	int		l;

	str = allocate_memory(n);
	if (!str)
		return (NULL);
	l = intlen(n);
	return (mini_itoa(str, l, n));
}
