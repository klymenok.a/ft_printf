/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_last_symbol.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 16:49:14 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 16:50:21 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_last_symbol(int i)
{
	char	*res;

	if (i == 0)
		res = ft_strdup("\0");
	else
	{
		res = (char *)malloc(sizeof(char) + 1);
		res[0] = i;
		res[1] = '\0';
	}
	return (res);
}
