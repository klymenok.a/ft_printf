/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_flags.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 11:36:28 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 16:43:37 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		hex_or_oct(int type)
{
	if (type == 5 || type == 6 || type == 7 || type == 8 || type == 10)
		return (1);
	return (0);
}

int		empty_flags(t_flags flags)
{
	if (flags.width == 0 && flags.size == 0 && flags.type == 0
			&& flags.flag[0] == '0' && flags.flag[1] == '0')
		return (0);
	return (1);
}

char	get_filler(t_flags flags)
{
	char	filler;

	if (flags.flag[4] == '1' && (flags.prec < 1 || flags.type == 1))
		filler = '0';
	else
		filler = ' ';
	return (filler);
}

int		some_check(t_flags flags)
{
	if (flags.flag[2] == '1' && empty_flags(flags) == 1 &&
			hex_or_oct(flags.type) == 0 && flags.type > 1 &&
			flags.type != 11 && flags.type != 12 && flags.nul == 0)
		return (1);
	return (0);
}

char	*get_flags(t_flags flags, size_t n, char **str, int width)
{
	int		memory;
	char	filler;
	char	*res;

	memory = 0;
	if ((int)n < width)
		memory += width - (int)n;
	if (memory > 0)
	{
		if (flags.type == 1 && str[4][0] == '\0')
			memory--;
		res = (char *)malloc(sizeof(char) * memory + 1);
		res[memory] = '\0';
		filler = get_filler(flags);
		while (memory-- >= 0)
			res[memory] = filler;
		if (flags.flag[2] == '1' && flags.type != 5)
			res[0] = ' ';
	}
	else if (memory == 0 && str[1][0] == '\0' && some_check(flags) == 1)
		res = ft_strdup(" ");
	else
		res = ft_strdup("\0");
	return (res);
}
