/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convertors.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 16:47:49 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 15:17:11 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		dec_to_octal(int n)
{
	int res;
	int i;

	res = 0;
	i = 1;
	while (n > 0)
	{
		res = res + (n % 8) * i;
		n = n / 8;
		i = i * 10;
	}
	return (res);
}

int		memory_for_new(uintmax_t value, int base)
{
	int memory;

	memory = 0;
	if (value == 0)
		return (1);
	while (value > 0)
	{
		value = value / base;
		memory++;
	}
	return (memory);
}

int		get_base(int type)
{
	if (type % 5 == 0)
		return (8);
	else
		return (16);
}

char	*convertor(uintmax_t n, int type)
{
	char		*s;
	char		*res;
	int			mem;
	int			i;
	uintmax_t	base;

	base = (uintmax_t)get_base(type);
	res = 0;
	i = 0;
	mem = memory_for_new(n, base);
	s = "0123456789abcdef";
	res = (char *)malloc(sizeof(char) * mem + 1);
	while (n >= base)
	{
		res[--mem] = s[n % base];
		n = n / base;
		i++;
	}
	res[--mem] = s[n % base];
	res[i + 1] = '\0';
	if (type == 6)
		res = ft_alllower(res);
	else if (type == 7)
		res = ft_allupper(res);
	return (res);
}
