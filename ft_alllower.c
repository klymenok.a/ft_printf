/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_alllower.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 22:46:27 by oklymeno          #+#    #+#             */
/*   Updated: 2017/01/30 16:09:54 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*ft_alllower(char *str)
{
	size_t	a;

	a = 0;
	while (str[a] != '\0')
	{
		if (str[a] > 64 && str[a] < 91)
			str[a] += 32;
		a++;
	}
	return (str);
}
