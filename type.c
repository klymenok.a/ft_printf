/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkers_for_flags_2.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 12:42:39 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 16:05:54 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		is_reading(char c)
{
	char	*str;
	int		i;

	str = "-+ 0#cCsSdDioOuUxXp.0123456789hlzj*";
	i = -1;
	while (str[++i] != '\0')
		if (c == str[i])
			return (1);
	return (0);
}

int		is_specifier(char c)
{
	char *specs;

	specs = "csdioxXpDOuUCS";
	while (*specs != '\0')
	{
		if (*specs == c)
			return (1);
		specs++;
	}
	return (0);
}

void	check_type(char *format, int i, t_flags *flags)
{
	char	*mass_types;
	int		n;

	i++;
	mass_types = "csdioxXpDOuUCS";
	while (is_specifier(format[i]) == 0 && is_reading(format[i]) == 1)
		i++;
	n = -1;
	while (mass_types[++n] != '\0')
		if (format[i] == mass_types[n])
			flags->type = n + 1;
}

void	check_percent(char *format, int i, t_flags *flags)
{
	i++;
	while (is_specifier(format[i]) == 0 && is_reading(format[i]) == 1)
		i++;
	if (flags->type == 0)
		flags->percent = format[i];
}
