/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type_define.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 17:18:13 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 17:26:44 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*str_type(t_flags flags, va_list ap)
{
	char	*tmp_str;
	char	tmp_char;
	char	*result;

	if (flags.size == 3)
		result = get_wchar(ap, flags);
	else
	{
		if (flags.type == 1)
		{
			tmp_str = 0;
			tmp_char = va_arg(ap, int);
			result = (char *)malloc(sizeof(char) * 2);
			result[0] = tmp_char;
			result[1] = '\0';
		}
		else
		{
			tmp_str = va_arg(ap, char*);
			tmp_char = 0;
			(tmp_str == NULL) ? (result = ft_strdup("(null)")) :
				(result = ft_strdup(tmp_str));
		}
	}
	return (result);
}

char	*signed_type_int(t_flags flags, va_list ap)
{
	intmax_t	x;
	char		*result;

	if (flags.type == 9)
		x = va_arg(ap, unsigned long int);
	else if (flags.size == 0)
		x = va_arg(ap, int);
	else if (flags.size == 2)
		x = (short)va_arg(ap, int);
	else if (flags.size == 3)
		x = va_arg(ap, long int);
	else if (flags.size == 5)
		x = va_arg(ap, intmax_t);
	else if (flags.size == 6)
		x = va_arg(ap, size_t);
	else if (flags.size == 1)
		x = (char)va_arg(ap, int);
	else
		x = va_arg(ap, long long int);
	result = ft_itoa(x);
	return (result);
}

char	*unsigned_type_with_size(t_flags flags, va_list ap)
{
	uintmax_t	x;
	char		*result;

	if (flags.size == 2 && flags.type == 12)
		x = va_arg(ap, unsigned long int);
	else if (flags.size == 2)
		x = (unsigned short)va_arg(ap, unsigned int);
	else if (flags.size == 3)
		x = va_arg(ap, unsigned long int);
	else if (flags.size == 5)
		x = va_arg(ap, uintmax_t);
	else if (flags.size == 6)
		x = va_arg(ap, size_t);
	else if (flags.size == 1 && (flags.type == 10 || flags.type == 12))
		x = va_arg(ap, unsigned int);
	else if (flags.size == 1)
		x = (unsigned char)va_arg(ap, unsigned int);
	else
		x = va_arg(ap, unsigned long long int);
	result = ft_itoa_u(x);
	return (result);
}

char	*unsigned_type(t_flags flags, va_list ap)
{
	uintmax_t	x;
	char		*result;

	if (flags.type >= 5 && flags.type <= 7)
		x = va_arg(ap, unsigned int);
	else if (flags.type == 8)
		x = va_arg(ap, unsigned long int);
	else if (flags.type == 9 || flags.type == 10)
		x = va_arg(ap, long int);
	else if (flags.type == 11)
		x = va_arg(ap, unsigned int);
	else if (flags.type == 12)
		x = va_arg(ap, unsigned long int);
	else if (flags.type == 13)
		x = va_arg(ap, unsigned long int);
	else
		x = va_arg(ap, unsigned long long int);
	result = ft_itoa_u(x);
	return (result);
}

char	*pointer(va_list ap)
{
	uintmax_t	x;
	char		*res;

	x = va_arg(ap, unsigned long int);
	res = ft_itoa_u(x);
	return (res);
}
