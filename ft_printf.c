/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/13 15:09:18 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 15:33:14 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	ft_printf(char *format, ...)
{
	va_list	ap;
	size_t	i;
	size_t	point;
	int		flag;
	t_flags	flags;

	g_output = 0;
	i = 0;
	flag = 0;
	va_start(ap, format);
	while (format[i] != '\0')
	{
		point = 0;
		if (format[i] == '%')
			i = i + checker(format, i, ap, &flags);
		else
		{
			write(1, &format[i], 1);
			g_output++;
		}
		i++;
	}
	va_end(ap);
	return (g_output);
}
