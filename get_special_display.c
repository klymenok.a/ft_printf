/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_special_display.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 16:40:16 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 16:43:20 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_spec(t_flags flags, char *str)
{
	char	*res;

	if (flags.flag[3] == '1' || flags.type == 8)
	{
		if (flags.type % 5 == 0 && str[0] != '0')
			res = ft_strdup("0");
		else if ((flags.type == 6 || flags.type == 7) && str[0] == '\0')
			res = ft_strdup("\0");
		else if (flags.type == 6 || flags.type == 8)
			res = ft_strdup("0x");
		else if (flags.type == 7)
			res = ft_strdup("0X");
		else
			res = "\0";
	}
	else
		res = "\0";
	return (res);
}
