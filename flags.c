/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 15:29:21 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 15:42:28 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	is_flag(char *format, int i)
{
	if (format[i] == '-' || format[i] == '+' || format[i] == ' ' ||
			format[i] == '#' || format[i] == '0')
		return (1);
	return (0);
}

static int	is_num_or_dot(char c)
{
	if ((c > 47 && c < 58) || c == 46)
		return (1);
	return (0);
}

void		check_flag(char *format, int i, t_flags *flags)
{
	int		n;
	char	*mass_flags;

	mass_flags = "-+ #0";
	while (is_specifier(format[++i]) == 0 && is_reading(format[i]) == 1)
	{
		n = -1;
		if (is_flag(format, i) == 1)
		{
			while (mass_flags[++n] != '\0')
			{
				if (format[i] == mass_flags[n])
				{
					if (format[i] == '0' && is_num_or_dot(format[i - 1]) == 0)
						flags->flag[n] = '1';
					else if (format[i] != '0')
						flags->flag[n] = '1';
				}
			}
		}
	}
	if (flags->flag[0] == '1' || (flags->prec >= 0 && flags->type > 2))
		flags->flag[4] = '0';
}
