/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   precision.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 15:54:10 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 15:57:30 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	is_prec(char *format, int i)
{
	int res;
	int n;

	res = 0;
	n = 0;
	while (is_specifier(format[i++]) == 0)
	{
		n++;
		if (format[i] == '.')
			res = n;
	}
	return (res);
}

void		check_precision(char *format, int i, t_flags *flags)
{
	int		n;
	int		m;
	int		prec;
	char	*str;

	prec = is_prec(format, i);
	m = 0;
	if (prec > 0)
	{
		flags->prec = 0;
		i += prec;
		n = i;
		while (format[++i] == '.')
			n++;
		while (ft_isdigit(format[i]) == 1)
			i++;
		(i > n) ? (str = get_number(format, n + 1)) :
			(str = ft_strdup("0"));
		flags->prec += ft_atoi(str);
		free(str);
	}
}
