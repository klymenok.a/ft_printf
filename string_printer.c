/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   values.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 11:04:44 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 17:45:36 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	order_init(int *order, int flags)
{
	int i;

	i = -1;
	while (flags > 0)
	{
		order[++i] = flags % 10;
		flags = flags / 10;
	}
}

void	get_order(int *order, t_flags flags)
{
	if (flags.flag[0] == '1' && flags.flag[1] == '1')
		order_init(order, 53241);
	else if ((flags.flag[0] == '0' || flags.flag[2] == '1') &&
			flags.flag[4] != '1')
		order_init(order, 43512);
	else if (flags.flag[0] == '1')
		order_init(order, 23451);
	else if (flags.flag[4] == '1' && flags.percent > 1)
		order_init(order, 45321);
	else if (flags.flag[4] == '1')
		order_init(order, 42531);
	else
		order_init(order, 32541);
}

int		check_void(char *str1, char *str2, t_flags flags)
{
	if (((str1[0] == '\0' || (str1[0] == '0' && str1[1] == '\0')) &&
			(str2[1] == 'x' || str2[1] == 'X' || flags.prec == -2)) &&
			flags.flag[3] == '1')
		return (1);
	return (0);
}

void	put_strings(char **result, t_flags flags)
{
	int	order[5];
	int	i;

	i = -1;
	while (++i < 5)
		order[i] = 0;
	if (flags.type > 0 || flags.percent > 0)
	{
		get_order(order, flags);
		i = 0;
		while (i < 5)
		{
			if (check_void(result[4], result[5], flags) == 1 && order[i] == 5)
				i++;
			else
			{
				if (flags.type == 1 && order[i] == 4 &&
						ft_strcmp(result[order[i]], "") == 0)
					ft_putstr_2(result[order[i++]]);
				else
					ft_putstr(result[order[i++]]);
			}
		}
	}
}
