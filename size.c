/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   size.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 16:00:41 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 16:01:08 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		is_size(char *format, int i)
{
	int	res;
	int	n;

	res = 0;
	n = 0;
	while (is_specifier(format[i]) == 0)
	{
		if (format[i] == 'h' || format[i] == 'l' || format[i] == 'j'
			|| format[i] == 'z')
		{
			res += n;
			return (res);
		}
		n++;
		i++;
	}
	return (res);
}

int		size_type(char *format, int i)
{
	if (format[i] == 'h' && format[i + 1] == 'h')
		return (1);
	else if (format[i] == 'h')
		return (2);
	else if (format[i] == 'l' && format[i + 1] == 'l')
		return (4);
	else if (format[i] == 'l')
		return (3);
	else if (format[i] == 'j')
		return (5);
	else if (format[i] == 'z')
		return (6);
	else
		return (0);
}

void	check_size(char *format, int i, t_flags *flags)
{
	int	n;
	int type;

	n = i + is_size(format, i);
	while (size_type(format, n) > 0)
	{
		type = size_type(format, n);
		if (type > 0)
		{
			if (flags->size < type)
				flags->size = type;
			if (type == 1 || type == 3)
				n++;
		}
		n++;
	}
}
