/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   width.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 15:48:40 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 19:59:04 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	is_width(char *str, int i)
{
	int	res;
	int	n;

	res = 0;
	n = 0;
	while (is_specifier(str[++i]) == 0 && is_reading(str[i]) == 1)
	{
		if (ft_isdigit(str[i]) == 1 && str[i - 1] != '.' && str[i] != '0')
		{
			while (ft_isdigit(str[i++]) == 1)
				res = n;
			break ;
		}
		n++;
	}
	return (res);
}

void		check_width(char *format, int i, t_flags *flags)
{
	int		n;
	int		m;
	int		x;
	char	a;
	char	*str;

	x = i;
	a = format[0];
	m = -1;
	n = i + is_width(format, i) + 1;
	if (n > i)
		str = get_number(format, n);
	else
		str = ft_strdup("\0");
	flags->width = ft_atoi(str);
	free(str);
}
