# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/13 15:04:05 by oklymeno          #+#    #+#              #
#    Updated: 2017/02/08 19:56:26 by oklymeno         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a
FLAGS = -Wall -Wextra -Werror
SOURCES = ft_printf.c\
		  get_output_string.c\
		  checkers.c ft_itoa.c\
		  ft_strlen.c ft_strdup.c\
		  ft_putstr.c\
		  ft_atoi.c\
		  ft_isdigit.c\
		  convertors.c\
		  ft_allupper.c\
		  ft_alllower.c\
		  ft_strjoin.c\
		  ft_bzero.c\
		  string_printer.c\
		  get_flags.c\
		  get_special_display.c\
		  get_last_symbol.c\
		  get_precision.c\
		  get_sign.c\
		  ft_itoa_u.c\
		  ft_putstr_2.c\
		  ft_strcmp.c\
		  flags.c\
		  width.c\
		  precision.c\
		  size.c\
		  type.c\
		  type_define.c\
		  wchar_type.c 
OBJECTS = $(SOURCES:.c=.o)

all: $(NAME)

$(NAME): $(OBJECTS)
	ar rc $(NAME) $(OBJECTS)

%.o: %.c
	gcc -c $(FLAGS) -o $@ $<

main:
	gcc -g $(SOURCES) $(FLAGS)  ../main.c -o print

clean:
	rm -f $(OBJECTS)

fclean: clean
	rm -f $(NAME)

re: fclean all
