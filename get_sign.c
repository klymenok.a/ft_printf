/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_sign.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 17:05:34 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 17:10:29 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_sign(char *str, t_flags flags)
{
	char	*result;

	if ((flags.type % 10 == 1 || flags.type % 10 == 2 ||
				hex_or_oct(flags.type) == 1) && str[0] != '-')
		result = ft_strdup("\0");
	else if (str[0] == '-' && flags.type != 1)
		result = ft_strdup("-");
	else if (flags.flag[1] == '1' && flags.type % 10 != 0 && flags.type != 6 &&
			flags.type != 7)
		result = ft_strdup("+");
	else
		result = ft_strdup("\0");
	return (result);
}
