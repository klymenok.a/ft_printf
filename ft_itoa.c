/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 19:58:06 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/02 20:31:44 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	intlen(intmax_t n)
{
	int		l;

	l = 0;
	if (n <= 0)
		l++;
	while (n != 0)
	{
		n = n / 10;
		l++;
	}
	return (l);
}

static char	*allocate_memory(intmax_t i)
{
	char	*str;

	str = (char *)malloc(intlen(i) + 1);
	if (!str)
		return (NULL);
	return (str);
}

static char	*mini_itoa(char *str, int l, intmax_t n)
{
	str[l] = '\0';
	if (n < 0)
	{
		str[0] = '-';
		while (l > 1)
		{
			str[--l] = n % 10 * -1 + '0';
			n = n / 10;
		}
	}
	else
	{
		while (l > 0)
		{
			str[--l] = n % 10 + '0';
			n = n / 10;
		}
	}
	return (str);
}

char		*ft_itoa(intmax_t n)
{
	char	*str;
	int		l;

	str = allocate_memory(n);
	if (!str)
		return (NULL);
	l = intlen(n);
	return (mini_itoa(str, l, n));
}
