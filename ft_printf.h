/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/13 15:09:36 by oklymeno          #+#    #+#             */
/*   Updated: 2017/02/08 19:57:53 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FT_PRINTF_H
# define __FT_PRINTF_H

# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <stdlib.h>
# include <stdarg.h>

int					g_output;
typedef	struct		s_flags
{
	char	*flag;
	int		width;
	int		prec;
	int		size;
	int		type;
	int		percent;
	int		nul;
}					t_flags;
size_t				ft_printf(char *format, ...);
int					dec_to_octal(int n);
char				*dec_to_hex(int value, int reg);
int					checker(char *format, int i, va_list ap, t_flags *flags);
void				get_output_string(t_flags *flags, va_list ap);
void				check_flag(char *format, int i, t_flags *flags);
void				check_width(char *format, int i, t_flags *flags);
void				check_precision(char *format, int i, t_flags *flags);
void				check_size(char *format, int i, t_flags *flags);
void				check_type(char *format, int i, t_flags *flags);
void				check_percent(char *format, int i, t_flags *flags);
void				ft_putstr(char const *s);
void				ft_putstr_2(char const *s);
char				*ft_strdup(const char *s1);
size_t				ft_strlen(char const *s);
char				*ft_itoa(intmax_t n);
int					ft_isdigit(int c);
uintmax_t			ft_atoi(char const *s);
char				*convertor(uintmax_t n, int type);
char				*ft_allupper(char *str);
char				*ft_alllower(char *str);
void				printer_1(int n, va_list ap);
char				*ft_strjoin(char const *s1, char const *s2);
void				ft_bzero(void *s, size_t n);
char				*get_value_to_string(t_flags flags, va_list ap);
void				put_strings(char **result, t_flags flags);
char				*get_flags(t_flags flags, size_t n, char **str, int width);
char				*get_last_symbol(int i);
char				*get_prec(char *str, t_flags flags, int p);
char				*get_sign(char *str, t_flags flags);
char				*ft_itoa_u(uintmax_t n);
char				*get_spec(t_flags flags, char *str);
int					hex_or_oct(int type);
int					ft_strcmp(char const *s1, char const *s2);
int					is_specifier(char c);
int					is_reading(char c);
char				*get_number(char *str, int start);
char				*str_type(t_flags flags, va_list ap);
char				*signed_type_int(t_flags flags, va_list ap);
char				*unsigned_type_with_size(t_flags flags, va_list ap);
char				*unsigned_type(t_flags flags, va_list ap);
char				*pointer(va_list ap);
char				*get_wchar(va_list ap, t_flags flags);
#endif
