/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oklymeno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 19:20:30 by oklymeno          #+#    #+#             */
/*   Updated: 2016/12/08 23:30:01 by oklymeno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*ft_strjoin(char const *s1, char const *s2)
{
	size_t	i;
	size_t	j;
	char	*new_str;

	i = -1;
	j = -1;
	if (s1 && s2)
	{
		new_str = (char *)malloc(ft_strlen(s1) + ft_strlen(s2) + 1);
		if (!new_str)
			return (NULL);
		while (s1[++j] != '\0')
			new_str[++i] = s1[j];
		j = -1;
		while (s2[++j] != '\0')
			new_str[++i] = s2[j];
		new_str[i + 1] = '\0';
		return (new_str);
	}
	return (NULL);
}
